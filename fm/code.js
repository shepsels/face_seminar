
var coupledImages = [
// hi dis hi mem
[ 1 , "Alan_Infante_11_oval.jpg" ],
[ 2 , "Google_1_Benjamin Mcshane_15_oval.jpg" ],
[ 3 , "Google_1_Grace Kuhns_1_oval.jpg" ],
[ 4 , "Google_1_Gwendolyn Vance_152_oval.jpg" ],
[ 5 , "Google_1_Harry Merriweather_3_oval.jpg" ],
[ 6 , "Google_1_John Woolf_11_oval.jpg" ],
[ 7 , "Google_1_Louis Tillis_19_oval.jpg" ],
[ 8 , "Google_1_Paul Keck_5_oval.jpg" ],
[ 9 , "Google_1_Russell Baeza_13_oval.jpg" ],
[ 10 , "Google_1_Steve Moyers_19_oval.jpg" ],
// hi_mem_lo_dis
[ 11 , "Google_1_Albert Fortunato_7_oval.jpg" ],
[ 12 , "Google_1_Benjamin Keck_17_oval.jpg" ],
[ 13 , "Google_1_Candice Varga_19_oval.jpg" ],
[ 14 , "Google_1_Douglas Ziegler_3_oval.jpg" ],
[ 15 , "Google_1_Eric Hawley_7_oval.jpg" ],
[ 16 , "Google_1_James Hannon_17_oval.jpg" ],
[ 17 , "Google_1_Maggie Schulz_11_oval.jpg" ],
[ 18 , "Google_1_Mark Harding_13_oval.jpg" ],
[ 19 , "Google_1_Roy Bieber_7_oval.jpg" ],
[ 20 , "Google_1_William Booker_3_oval.jpg" ],
// lo_mem_hi_dis
[ 21 , "Google_1_Carlos Coker_9_oval.jpg" ],
[ 22 , "Google_1_Carlos Mellott_7_oval.jpg" ],
[ 23 , "Google_1_Daniel Goodman_9_oval.jpg" ],
[ 24 , "Google_1_Donald Naquin_1_oval.jpg" ],
[ 25 , "Google_1_Frank Bivins_15_oval.jpg" ],
[ 26 , "Google_1_Joshua Handley_1_oval.jpg" ],
[ 27 , "Google_1_Mike Boatwright_1_oval.jpg" ],
[ 28 , "Google_1_Rodney Lanning_3_oval.jpg" ],
[ 29 , "Google_1_Shawn Charles_1_oval.jpg" ],
[ 30 , "Google_1_Wayne Carnes_3_oval.jpg" ],
// lo_mem_lo_dis
[ 31 , "Adam_Koester_6_oval.jpg" ],
[ 32 , "Eric_Barbour_17_oval.jpg" ],
[ 33 , "Google_1_Alberta Gilliard_3_oval.jpg" ],
[ 34 , "Google_1_Christopher Hazard_1_oval.jpg" ],
[ 35 , "Google_1_Harry Hickman_15_oval.jpg" ],
[ 36 , "Google_1_Jack Province_9_oval.jpg" ],
[ 37 , "Google_1_Matthew Ennis_11_oval.jpg" ],
[ 38 , "Google_1_Nathan Lusk_17_oval.jpg" ],
[ 39 , "Google_1_Norman Westfall_5_oval.jpg" ],
[ 40 , "Google_1_Roy Cross_9_oval.jpg" ],
];

var blankImgName = "https://lh4.googleusercontent.com/-5hHasg1CjHE/UCPKvxaZ2pI/AAAAAAAAArM/zKVVCw6hpsw/s128/blank.jpg";

var baseImages = ["0"]

// var tImgs = ["0",
//     "https://lh3.googleusercontent.com/-vXrd7zJLk00/UCPKwfFGJOI/AAAAAAAAArU/OuLE_Rj7w-o/h120/homer1.jpg",
//     "https://lh4.googleusercontent.com/-31dHYOJyBE4/UCPKws0iZ8I/AAAAAAAAArY/Qz2WOka5uHU/h120/homer2.jpg",
//     "https://lh4.googleusercontent.com/-hbLpDLN51fI/UCPKv4YZ3jI/AAAAAAAAArc/KANqng-tnmE/h120/bart.jpg",
//     "https://lh5.googleusercontent.com/-KZE2YvH_BYI/UCPKwx8SgnI/AAAAAAAAArk/DNefP53RSsg/h120/lisa.jpg",
//     "https://lh3.googleusercontent.com/-tnoh0dx-gA4/UCPKv0nQ55I/AAAAAAAAArQ/U1ru4gKNees/h120/clark.jpg",
//     "https://lh5.googleusercontent.com/-illwu8DeSLk/UCPKydXE8II/AAAAAAAAAr4/gosXuBm4hTw/h120/superman.jpg"];

var namesWithEvents = [
["אלון", 84],
["בן", 67],
["גדי", 68],
["דוד", 83],
["ירון", 72],
["לירן", 75],
["מאור", 78],
["ניר", 66],
["עומר", 71],
["תמיר", 188]];

function getNamesOrValues(res, arr, number) {
    for (i = 0; i<arr.length; i++) {
        res.push(arr[i][number]);
    }
    return res;
}

var namesNoZero = [
    "אלון",
    "בן",
    "גדי",
    "דוד",
    "ירון",
    "לירן",
    "מאור",
    "ניר",
    "עומר",
    "תמיר",
];

var names = [];


var namekeys = [0,
    84,
    67,
    68,
    83,
    86,
    90,
    74,
    89,
    72,
    70,
    75,
    78,
    66,
    88,
    71,
    80,
    77,
    82,
    65,
    188,
    84,
    67,
    68,
    83,
    74,
    72,
    78,
    66,
    71,
    65];



// familiarization: we have 30 pictures to learn, in 3 stages of 10

// each stage includes 5 phases

// the lists are per phase: tList1 is for phase 1, tList2 is for phase 2 etc.

// in each list, we have 3 picture lists, 1 for each stage



var tLists1 = [[0],

    [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ],
    [ 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 ],
    [ 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 ],
    [ 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40 ]
    ];


var tLists2 = [[0],

    [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ],
    [ 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 ],
    [ 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 ],
    [ 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40 ]
    ];


var tLists3 = [[0],

    [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ],
    [ 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 ],
    [ 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 ],
    [ 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40 ]
    ];

var tLists4 = [[0],

    [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ],
    [ 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 ],
    [ 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 ],
    [ 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40 ]
    ];


var tLists5 = [[0],

    [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ],
    [ 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 ],
    [ 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 ],
    [ 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40 ]
    ];



// general variables

var curTrialList;

var resultString = "Familiarization: ";



// familiarization variables
// todo change numbers
var p1Steps = 50; // 50;

var p2Steps = 10; // 10

var p3Steps = 50; // 50;

var p4Steps = 60; // 60;

var p5Steps = 60; // 60;

if (commonFun.getQueryVariable('debug') == "true") {
    ////////////////////
    // Debug settings //
    ////////////////////
    var p1DispTime = 200; // 2000;
    var p1ISI = 100; // 1000;

    var p2ISI = 100; // 2000;

    var p3ISI1 = 200;
    var p3ISI2 = 100;

    var p4ISI = 100;

    var p5ISI1 = 100;
    var p5ISI2 = 100;

} else {
    ///////////////////////
    // Original settings //
    ///////////////////////
    var p1DispTime = 2000; // 2000;
    var p1ISI = 1000; // 1000;

    var p2ISI = 1000; // 2000;

    var p3ISI1 = 2000;
    var p3ISI2 = 1000;

    var p4ISI = 1000;

    var p5ISI1 = 1000;
    var p5ISI2 = 1000;
}


var p5Cond = 0;

var picNum = 0;
var imId = 0;
var stage = 1;
var numStages = 3;
var numFaces = 10; // 30;



var dbgstr = "";
var keynum;

var fakeResults = [
    ["hd1", "hd2", "hd3", "hd4"],
    ["text"],
    ["hd1"],
    ["hd2"],
    ["hd1"],
    ["hd3"],
    ["text"],
    ["hd4"],
    ["hd1"],
    ["text"],
    ["hd1"]
];



// lineup variables

var mode = 0; // 0 - simultaneous matching, 1 - sequential matching
var trialNum = 0; // init at 0, incerement every time the button is pressed
var onBreak = 0;
var maxTrials = 90; // 90;
var isPractice = 1;
var practiceTrialNum = 0;
var numPictures = 30;
var numSubjects = 25;

//var subjectId = 1;
var trialId;
var firstImgId;
var secondImgId;
var firstImgName;
var secondImgName;
var img1;
var img2;
var buttonName;
var progressStr;
var scrnH;
var scrnW;
var leftMargin1;
var leftMargin2;
var topMargin1;
var topMargin2;


var experimentName, experimenterName, subjectId;

experimentName = "seminar_fm";
experimenterName = "seminar";
subjectId = commonFun.randomstring(8);


// Familiarization Functions

// Global design:

// we train for 30 faces, in 3 stages of 10 faces each.

// for each subject the faces order is random.

// after each stage of 10 we give a break, and start again.

// Phase 1:

// Show the 10 faces on the screen, sequentially, each for p1DispTime seconds.

// Above each face there is a name.

// Repeat p1Repeats times.

// Phase 2:

// Show faces with names again, and wait till subject presses the first letter of each name.

// No repetitions.

// Phase 3:

// Show faces without names. wait till subject presses the first letter of each name.

// If the subject is wrong, show the correct name.

// Repeat p3Repeats times.

// Phase 4: like phase 3, without correction.

// Repeat p4Repeats times.

// Phase 5: show name for p5DispTime, wait p5ISI, then face, and wait till subject presses 0

// if he thinks for match and 1 for mismatch.

// Repeat p5Repeats times.


function shuffle(a) {
    var j, x, i;
    for (i = a.length; i; i--) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
}


function initExperiment(phase)

{
    var msg;
    var rand;

    // shuffle names and than create array of names and array of events
    shuffle(namesWithEvents);
    getNamesOrValues(names, namesWithEvents, 0);
    namekeys = [];
    getNamesOrValues(namekeys, namesWithEvents, 1);
    getNamesOrValues(baseImages, coupledImages, 1);

    document.getElementById('stageBreak').style.display = 'none';
    document.getElementById('general-instructions').style.display = 'none';

    stage = parseInt(commonFun.getQueryVariable('stage'));

    if (!stage || [1, 2, 3, 4].indexOf(stage) === -1) {

        stage = Math.floor(Math.random() * 4) + 1;  
    }

    experimentName = experimentName + "_stage_" + stage;

    console.log('running stage: ' + stage);

    switch (phase)

    {

        case 1:

            document.getElementById('p1-instructions').style.display = 'block';
            break;
        case 2:

            document.getElementById('p2-instructions').style.display = 'block';
            break;
        case 3:

            document.getElementById('p3-instructions').style.display = 'block';
            break;
        case 4:

            document.getElementById('p4-instructions').style.display = 'block';
            break;
        default:

            document.getElementById('p5-instructions').style.display = 'block';
    }

}





// Phase 1

function startPhase1()

{

    document.getElementById('p1-instructions').style.display = 'none';
    picNum = 0;
    // take the picture order list

    shuffle(tLists1[stage]);
    curTrialList = tLists1[stage];
    document.getElementById('phase1').style.display = 'block';
    showNextPictureP1();
}



function showNextPictureP1()

{

    if (picNum >= p1Steps) {

        document.getElementById('phase1').style.display = 'none';
        document.getElementById('p2-instructions').style.display = 'block';
    } else {

        imId = curTrialList[picNum];
        document.getElementById('p1-name').innerHTML = names[imId%10];
        document.getElementById('p1-img').src = baseImages[imId];
        document.getElementById('p1-img').alt = String(baseImages[imId]);
        document.getElementById('p1-img').style.display = 'block';
        document.getElementById('p1-name').style.display = 'block';
        ++picNum;
        setTimeout("startP1ISI()", p1DispTime);
    }

}



function startP1ISI()

{

    document.getElementById('p1-name').style.display = 'none';
    document.getElementById('p1-img').style.display = 'none';
    setTimeout("showNextPictureP1()", p1ISI);
}

// end phase 1

// Phase 2

function startPhase2()

{

    document.getElementById('p2-instructions').style.display = 'none';
    picNum = 0;
    // take the picture order list

    shuffle(tLists2[stage]);
    curTrialList = tLists2[stage];
    document.getElementById('phase2').style.display = 'block';
    showNextPictureP2();
}



function showNextPictureP2()

{

    if (picNum >= p2Steps) {

        document.getElementById('phase2').style.display = 'none';
        document.getElementById('p3-instructions').style.display = 'block';
    } else {

        imId = curTrialList[picNum];
        ++picNum;
        document.getElementById('p2-name').innerHTML = names[imId%10];
        document.getElementById('p2-name').style.display = 'block';
        document.getElementById('p2-img').src = baseImages[imId];
        document.getElementById('p2-img').alt = String(baseImages[imId]);
        document.getElementById('p2-img').style.display = 'block';
        document.getElementById('p2-input').innerHTML = '<input type="text" onkeypress="getUserKeyPressP2()" autofocus>';
        document.getElementById('p2-input').style.display = 'block';
        document.querySelector('#p2-input input').focus();
    }

}



function getUserKeyPressP2()

{

    setTimeout("goToP2ISI()", p2ISI);
}



function goToP2ISI()

{

    document.getElementById('p2-name').style.display = 'none';
    document.getElementById('p2-img').style.display = 'none';
    document.getElementById('p2-input').style.display = 'none';
    setTimeout("showNextPictureP2()", p2ISI);
}

// end phase 2



// Phase 3

function startPhase3()

{

    document.getElementById('p3-instructions').style.display = 'none';
    picNum = 0;
    // take the picture order list

    shuffle(tLists3[stage]);
    curTrialList = tLists3[stage];
    document.getElementById('phase3').style.display = 'block';
    showNextPictureP3();
}



function showNextPictureP3()

{

    if (picNum >= p3Steps) {

        setTimeout("goToP4()", p3ISI1);
    } else {

        imId = curTrialList[picNum];
        ++picNum;
        document.getElementById('p3-img').src = baseImages[imId];
        document.getElementById('p3-img').alt = String(baseImages[imId]);
        document.getElementById('p3-img').style.display = 'block';
        document.getElementById('p3-input').innerHTML = '<input type="text" onkeyup="getUserKeyPressP3(event)" autofocus>';
        document.getElementById('p3-input').style.display = 'block';
        document.querySelector('#p3-input input').focus();
    }

}



function getUserKeyPressP3(event)

{

    // find out which key was pressed

    keynum = event.which;
    console.log(keynum);
    if (event.which != namekeys[imId%10]) {

        setTimeout("showP3Feedback()", p3ISI1);
    } else {

        setTimeout("goToP3ISI()", p3ISI1);
    }

}



function showP3Feedback()

{

//    dispDbg(336);

    document.getElementById('p3-input').style.display = 'none';
    document.getElementById('p3-name').innerHTML = names[imId%10];
    document.getElementById('p3-name').style.display = 'block';
    setTimeout("goToP3ISI()", p3ISI1);
}



function goToP3ISI()

{

    document.getElementById('p3-name').style.display = 'none';
    document.getElementById('p3-input').style.display = 'none';
    document.getElementById('p3-img').style.display = 'none';
    setTimeout("showNextPictureP3()", p3ISI2);
}



function goToP4()

{

    document.getElementById('phase3').style.display = 'none';
    document.getElementById('p3-name').style.display = 'none';
    document.getElementById('p3-input').style.display = 'none';
    document.getElementById('p3-img').style.display = 'none';
    document.getElementById('p4-instructions').style.display = 'block';
}

// end phase 3



// Phase 4

function startPhase4()

{

    document.getElementById('p4-instructions').style.display = 'none';
    // take the picture order list

    shuffle(tLists4[stage]);
    curTrialList = tLists4[stage];
    document.getElementById('phase4').style.display = 'block';
    picNum = 0;
    showNextPictureP4();
}



function showNextPictureP4()

{

    if (picNum >= p4Steps) {

        document.getElementById('phase4').style.display = 'none';
        document.getElementById('p5-instructions').style.display = 'block';
    } else {

        imId = curTrialList[picNum];
        ++picNum;
        document.getElementById('p4-img').src = baseImages[imId];
        document.getElementById('p4-img').alt = String(baseImages[imId]);
        document.getElementById('p4-img').style.display = 'block';
        document.getElementById('p4-input').innerHTML = '<input type="text" onkeyup="getUserKeyPressP4(event)" autofocus>';
        document.getElementById('p4-input').style.display = 'block';
        document.querySelector('#p4-input input').focus();
    }

}

function getUserKeyPressP4() {
    setTimeout("goToP4ISI()", p4ISI);
}

function goToP4ISI() {

    document.getElementById('p4-img').style.display = 'none';
    document.getElementById('p4-input').style.display = 'none';
    setTimeout("showNextPictureP4()", p4ISI);
}

// end phase 4

// Phase 5

function startPhase5() {

    document.getElementById('p5-instructions').style.display = 'none';
    // take the picture order list

    shuffle(tLists5[stage]);
    curTrialList = tLists5[stage];
    document.getElementById('phase5').style.display = 'block';
    picNum = 0;
    showNextNameP5();
}



function showNextNameP5() {

    document.getElementById('p5-input').style.display = 'none';
    document.getElementById('p5-img').style.display = 'none';
    if (picNum >= p5Steps) {

        // ++stage;
        // if (stage <= numStages) {
        //
        //     // go to break page - between stages
        //
        //     document.getElementById('phase5').style.display = 'none';
        //     document.getElementById('stageBreak').style.display = 'block';
        // } else {
        //
        //     // go to lineup
        //
        //     document.getElementById('phase5').style.display = 'none';
        //     document.getElementById('lineup-instructions').style.display = 'block';
        // }

        document.getElementById('phase5').style.display = 'none';
        //        document.getElementById('lineup-instructions').style.display = 'block';


        // todo delete:
        console.log(prepareResultArray(resultString));
        commonFun.runGenericEndOfExpRoutine(experimentName, experimenterName, subjectId, prepareResultArray(resultString));

    } else {

         imId = curTrialList[picNum];
        // decide if this is a "same" or "diff" condition

        p5Cond = (Math.random() > 0.5) ? 48 : 49;
        var curName;
        if (p5Cond == 48) {
            // "same"
            curName = names[imId%10];
        } else {
            // "diff" - select a random name
            var diffNameId = Math.floor(numFaces * (Math.random()) + 1);
            curName = names[diffNameId%10];
        }

        document.getElementById('p5-name').innerHTML = curName;
        document.getElementById('p5-name').style.display = 'block';

        setTimeout("showNextPictureP5()", p5ISI1);
    }
}

function showNextPictureP5() {

    document.getElementById('p5-name').style.display = 'none';
    document.getElementById('p5-img').src = baseImages[imId];
    document.getElementById('p5-img').alt = String(baseImages[imId]);
    document.getElementById('p5-img').style.display = 'block';
    commonFun.startTimer();
    ++picNum;
    resultString = resultString.concat(imId.toString());
    resultString = resultString.concat(" - ");
    document.getElementById('p5-input').innerHTML = '<input type="text" onkeyup="getUserKeyPressP5(event)" autofocus>';
    document.getElementById('p5-input').style.display = 'block';
    document.querySelector('#p5-input input').focus();
}



function getUserKeyPressP5(event)

{

    // find out which key was pressed
    time = commonFun.stopTimer();

    // hit
    if (event.which === 48 && p5Cond === 48) {
        resultString = resultString.concat(" h - ", time, ",");
        setTimeout("showNextNameP5()", p5ISI2);
    }
    // miss
    else if(event.which < p5Cond) {
        resultString = resultString.concat(" m - ", time, ",");
        setTimeout("showNextNameP5()", p5ISI2);
    }
    // correct rejection
    else if(event.which === 49 && p5Cond === 49) {
        resultString = resultString.concat(" cr - ", time, ",");
        setTimeout("showNextNameP5()", p5ISI2);
    }
    // false alarm
    else if(event.which > p5Cond) {
        resultString = resultString.concat(" fa - ", time, ",");
        setTimeout("showNextNameP5()", p5ISI2);
    }


//     if (event.which != p5Cond) {

// // wrong answer
//         resultString = resultString.concat(" w - ", time, ",");
//         setTimeout("showNextNameP5()", p5ISI2);
//     } else {

//         resultString = resultString.concat(" c - ", time, ",");
//         setTimeout("showNextNameP5()", p5ISI2);
//     }

}

// end phase 5



function dispDbg(line)

{

    dbgstr = dbgstr.concat(line.toString());
//    dbgstr = dbgstr.concat(", p-");

//    dbgstr = dbgstr.concat(picNum.toString());

//    dbgstr = dbgstr.concat(", i-");

//    dbgstr = dbgstr.concat(imId.toString());

//    dbgstr = dbgstr.concat(", k-");

//    dbgstr = dbgstr.concat(keynum.toString());

//    dbgstr = dbgstr.concat("; ");

//    document.getElementById('dbg').innerHTML = dbgstr;

    document.getElementById('dbg').innerHTML = resultString;
}





// Lineup Functions

// there are 3 conditions:

// same: base vs. ref

// changed: modified vs. ref

// other: other vs. ref

// we have 3 lists of pictures: ref, base and mod.

// for the same condition we take a ref picture and the corresponding base picture.

// for the changed condition we take a ref picture and the corresponding mod picture.

// for the other condition we take a ref picture and another ref picture (add "sub_id" to the

// picture index).

// to select a condition we have a random perm of 1-90: 1-30 - same, 31-60 - changed,

// 61-90 - other.

// we take the number mod 30, and that will be the ref picture index.



function initExp() {
    resultString = resultString.concat("Lineup: ");
    // select a randon image order list (there are numVersions possible lists)

    var curTrialListID = Math.floor(numSubjects * (Math.random()) + 1);
    curTrialList = trialLists[curTrialListID];
    // debug

    //    curTrialList = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90];

    //,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120];

    document.getElementById('expStartReady').style.display = 'none';
    document.getElementById('expStartButton').style.display = 'none';
    showImages();
}



function showImages() {

    if (trialNum >= maxTrials) {

        showFinalPage();
    } else {

        if (onBreak == 1) {

            document.getElementById('breakPage').style.display = 'none';
            onBreak = 0;

        }

        trialId = curTrialList[trialNum];

        if (trialId <= numPictures) {

            // "same" condition:

            firstImgName = refImages[trialId];
            secondImgName = baseImages[trialId];
            resultString = resultString.concat("t: "); // trial num

            resultString = resultString.concat(trialNum.toString()); // trial num

            resultString = resultString.concat(",c: s"); // condition

            resultString = resultString.concat(",i1: "); // image 1

            resultString = resultString.concat(trialId); // image 1

            resultString = resultString.concat(",i2: "); // image 2

            resultString = resultString.concat(trialId); // image 2

        } else if (trialId > numPictures && trialId <= 2 * numPictures) {

            // "changed" condition:

            imgId = trialId - numPictures;
            firstImgName = refImages[imgId];
            secondImgName = modImages[imgId];
            resultString = resultString.concat("t: "); // trial num

            resultString = resultString.concat(trialNum.toString()); // trial num

            resultString = resultString.concat(",c: c"); // condition

            resultString = resultString.concat(",i1: "); // image 1

            resultString = resultString.concat(imgId); // image 1

            resultString = resultString.concat(",i2: "); // image 2

            resultString = resultString.concat(imgId); // image 2

        } else {

            // "other" condition

            imgId = trialId - (2 * numPictures);
//            secImgId = imgId + subjectId;

            secImgId = imgId + 1;
            if (secImgId > numPictures) {

                secImgId = secImgId - numPictures;
            }

            // select picture list

            var condStr = ",c: o";

            var firstList;

            var secondList;

            var opt = imgId % 3;

            switch (opt)

            {

                case 0: // both are ref images

                    firstList = refImages;
                    secondList = refImages;
                    condStr = condStr.concat("rr");
                    break;
                case 1: // both are mod images

                    firstList = modImages;
                    secondList = modImages;
                    condStr = condStr.concat("mm");
                    break;
                default: // one ref, one mod

                    firstList = refImages;
                    secondList = modImages;
                    condStr = condStr.concat("mr");
            }



            firstImgName = firstList[imgId];

            secondImgName = secondList[secImgId];



            resultString = resultString.concat("t: "); // trial num

            resultString = resultString.concat(trialNum.toString()); // trial num

            resultString = resultString.concat(condStr); // condition

            resultString = resultString.concat(",i1: "); // image 1

            resultString = resultString.concat(imgId); // image 1

            resultString = resultString.concat(",i2: "); // image 2

            resultString = resultString.concat(secImgId); // image 2

        }



        var compSide = (Math.random() > 0.5) ? 0 : 1;

        if (compSide == 0) {

            img1 = firstImgName;
            img2 = secondImgName;
        } else {

            img2 = firstImgName;
            img1 = secondImgName;
        }



        trialNum = ++trialNum;



        // update progress

        progressStr = trialNum.toString();

        progressStr = progressStr.concat(" of ");

        progressStr = progressStr.concat(maxTrials.toString());



        if (mode == 0) {

//            setTimeout("loadSimImages()",1000);

            loadSimImages();
        } else {

            loadSeqImages();
        }

    }

}



function loadSimImages()

{

    document.getElementById('sim-image1').style.display = 'none';
    document.getElementById('sim-image2').style.display = 'none';
    document.getElementById('blank').style.display = 'none';
    document.getElementById('sim-buttons').style.display = 'none';
    document.getElementById('sim-progress-status').style.display = 'none';
    // set images data

    document.getElementById('sim-image1').src = img1;
    document.getElementById('sim-image2').src = img2;
    document.getElementById('sim-image1').alt = String(img1);
    document.getElementById('sim-image2').alt = String(img2);
    setTimeout("showSimImages()", 1000);
}



function showSimImages()

{

    document.getElementById('sim-image1').style.display = 'block';
    document.getElementById('sim-image2').style.display = 'block';
    document.getElementById('blank').style.display = 'block';
    //    document.getElementById('sim-buttons').innerHTML = '<p class = "abutton"><input type="radio" onClick="testResponse(1)"  name="'+buttonName+'" value="1"/><b>1.בטוח אנשים שונים</b><br><input type="radio" onClick="testResponse(2)" name="'+buttonName+'" value="2" /><b>2.אנשים שונים</b><br><input type="radio" onClick="testResponse(3)" name="'+buttonName+'" value="3"/><b>3.כנראה אנשים שונים</b><br><input type="radio" onClick="testResponse(4)" name="'+buttonName+'" value="4" /><b>4.כנראה אותו אדם</b><br><input type="radio" onClick="testResponse(5)" name="'+buttonName+'" value="5" /><b>5.אותו אדם</b><br><input type="radio" onClick="testResponse(6)" name="'+buttonName+'" value="6" /><b>6.בטוח אותו אדם</b></p>';



    document.getElementById('sim-buttons').innerHTML = '<table><tr><td><input type="radio" value = "1" onClick="testResponse(1)"/><b><br>1.בטוח <br>אותו אדם</b></td><td>--</td><td><input type="radio" value = "1" onClick="testResponse(2)"/><b><br>2.<br>אותו אדם</b></td><td>--</td><td><input type="radio" value = "1" onClick="testResponse(3)"/><b><br>3.כנראה <br>אותו אדם</b></td><td>--</td><td><input type="radio" value = "1" onClick="testResponse(4)"/><b><br>4.כנראה <br>אנשים שונים</b></td><td>--</td><td><input type="radio" value = "1" onClick="testResponse(5)"/><b><br>5.<br>אנשים שונים</b></td><td>--</td><td><input type="radio" value = "1" onClick="testResponse(6)"/><b><br>6.בטוח <br>אנשים שונים</b></td></tr></table>';
    document.getElementById('sim-buttons').style.display = 'block';
    // display progress

    if (isPractice == 0) {

        document.getElementById('sim-progress-status').innerHTML = progressStr;
        document.getElementById('sim-progress-status').style.display = 'block';
    }

}



function loadSeqImages()

{

    // define random locations

    var leftRight = (Math.random() > 0.5) ? 1 : (-1);
    var upDown = (Math.random() > 0.5) ? 1 : (-1);
    leftMargin1 = ((scrnW / 2.8) + Math.random() * (scrnW / 20) * leftRight).toString();
    topMargin1 = ((scrnH / 20) + Math.random() * (scrnH / 20) * upDown).toString();
    leftRight = (Math.random() > 0.5) ? 1 : (-1);
    upDown = (Math.random() > 0.5) ? 1 : (-1);
    leftMargin2 = ((scrnW / 2.8) + Math.random() * (scrnW / 10) * leftRight).toString();
    topMargin2 = ((scrnH / 20) + Math.random() * (scrnH / 20) * upDown).toString();
    document.getElementById('seq-image').hspace = leftMargin1;
    document.getElementById('seq-image').vspace = topMargin1;
    document.getElementById('seq-image').src = img1;
    document.getElementById('seq-image').alt = String(img1);
    setTimeout("showSeqImage1()", 1500);
}



function showSeqImage1()

{

    document.getElementById('seq-buttons').style.display = 'none';
    document.getElementById('seq-progress-status').style.display = 'none';
    document.getElementById('seq-image').style.display = 'block';
    document.getElementById('seq-images').style.display = 'block';
    setTimeout("goITI()", 1000);
}



function goITI()

{

    // 1 second blank interval

    document.getElementById('seq-image').src = blankImgName;
    document.getElementById('seq-image').alt = "Error";
    document.getElementById('seq-image').style.display = 'none';
    // random location

    document.getElementById('seq-image').hspace = leftMargin2;
    document.getElementById('seq-image').vspace = topMargin2;
    document.getElementById('seq-image').src = img2;
    document.getElementById('seq-image').alt = String(img2);
    setTimeout("showSeqImage2()", 1000);
}



function showSeqImage2()

{

    document.getElementById('seq-image').style.display = 'block';
    document.getElementById('seq-images').style.display = 'block';
    setTimeout("seqPreChoiceInterval()", 1000);
}



function seqPreChoiceInterval()

{

    // 1 second interval, clear image

    document.getElementById('seq-image').src = blankImgName;
    document.getElementById('seq-image').alt = "Error";
    document.getElementById('seq-image').style.display = 'none';
    setTimeout("showSeqButtons()", 1000);
}



function showSeqButtons()

{

    document.getElementById('seq-buttons').innerHTML = '<p class = "abutton"><input type="radio" onClick="testResponse(1)"  name="' + buttonName + '" value="1"/><b>1.בטוח אנשים שונים</b><br><input type="radio" onClick="testResponse(2)" name="' + buttonName + '" value="2" /><b>2.אנשים שונים</b><br><input type="radio" onClick="testResponse(3)" name="' + buttonName + '" value="3"/><b>3.כנראה אנשים שונים</b><br><input type="radio" onClick="testResponse(4)" name="' + buttonName + '" value="4" /><b>4.כנראה אותו אדם</b><br><input type="radio" onClick="testResponse(5)" name="' + buttonName + '" value="5" /><b>5.אותו אדם</b><br><input type="radio" onClick="testResponse(6)" name="' + buttonName + '" value="6" /><b>6.בטוח אותו אדם</b></p>';
    document.getElementById('seq-buttons').style.display = 'block';
    document.getElementById('seq-images').style.display = 'block';
    // show progress:

    if (isPractice == 0) {

        document.getElementById('seq-progress-status').innerHTML = progressStr;
        document.getElementById('seq-progress-status').style.display = 'block';
    }

}



function testResponse(res)

{

    if (isPractice == 0) {

// save result string

        resultString = resultString.concat(",r: ");
        resultString = resultString.concat(res.toString());
        resultString = resultString.concat(", ");
    }

    setTimeout("isBreakTime()", 1000);

}



function isBreakTime()

{

    // no need for breaks

//    if(trialNum==maxTrials/4 || trialNum==(maxTrials/2) || trialNum==(maxTrials*3/4)) {

    if (trialNum == (maxTrials / 2)) {

        onBreak = 1;
        document.getElementById('sim-image1').style.display = 'none';
        document.getElementById('sim-image2').style.display = 'none';
        document.getElementById('blank').style.display = 'none';
        document.getElementById('sim-buttons').style.display = 'none';
        document.getElementById('sim-progress-status').style.display = 'none';
        document.getElementById('seq-buttons').style.display = 'none';
        document.getElementById('sim-buttons').style.display = 'none';
        document.getElementById('seq-progress-status').style.display = 'none';
        document.getElementById('seq-images').style.display = 'none';
        document.getElementById('breakPage').style.display = 'block';
    } else {

        if (isPractice == 1) {

            practice();
        } else {

            showImages();
        }

    }

}



function showFinalPage()

{
    commonFun.runGenericEndOfExpRoutine(experimentName, experimenterName, subjectId, [resultString]);

 }



function practice()

{

    // set screen parameters

    scrnH = screen.availHeight;
    scrnW = screen.availWidth;
    // hide instructions page

    document.getElementById('lineup-instructions').style.display = 'none';
    if (practiceTrialNum > 4) {

//    if(1) {

        setTimeout("endPractice()", 1000);
    } else {

        img1 = tImgs[++practiceTrialNum];
        img2 = tImgs[++practiceTrialNum];
        firstImgId = 0;
        secondImgId = 0;
        buttonName = "b";
        if (mode == 0) {

            setTimeout("loadSimImages()", 1000);
        } else {

            loadSeqImages();
        }

    }

}



function endPractice() {
    isPractice = 0;
    document.getElementById('seq-buttons').style.display = 'none';
    document.getElementById('sim-buttons').style.display = 'none';
    document.getElementById('sim-image1').style.display = 'none';
    document.getElementById('sim-image2').style.display = 'none';
    document.getElementById('blank').style.display = 'none';
    document.getElementById('seq-images').style.display = 'none';
    document.getElementById('expStartReady').style.display = 'block';
}

function loadAllImages() {
    var i, div, img;

    div = document.createElement('div');
    div.style.display = "none";
    document.body.appendChild(div);

    for (i = 1; i < baseImages.length; i++) {
        img = document.createElement('img');
        img.src = baseImages[i];
        div.appendChild(img);
    }
}

function prepareResultArray(data) {
    var i, result, raw_data;

    result = [];

    result.push([experimentName, experimenterName, subjectId]);

    raw_data = data.split(',');

    for (i = 0; i < raw_data.length; i++) {
        result.push([raw_data[i]]);
    }

    return result;
}