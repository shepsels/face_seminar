"use strict";

window.commonFun = window.commonFun || {};

commonFun.CONSTS = {
    testSubjectPrefix: "test_"
};

// Used to get the filenames of stimuli in a folder.
commonFun.getFilenamesInFolder = function (pathToExp, imageDirName, fileExtension, callbackFun) {
    var postData, fullPath;

    fullPath = pathToExp + imageDirName;
    // Protect againt double "//" in unix
    fullPath = fullPath.replace("//", "/");
    // Protect againt double "//" in windows
    fullPath = fullPath.replace("\\\\", "\\");

    // Call the server.
    postData = $.post(commonFun.getRouterURL(), {
        action: 'getFilenamesInFolder', folder: fullPath, extension: fileExtension
    });

    // When we have received the filenames, parse the respose as a JSON
    // response and call callbackFun.
    postData.done(function (response) {
        var i, filenames;

        filenames = JSON.parse(response);

        // remove the path to the experiment from the filename.
        for (i = 0; i < filenames.length; i++) {
            filenames[i] = filenames[i].replace(pathToExp, "");
        }

        callbackFun(filenames);
    });
};

commonFun.verticallyCenterElemInWindow = function (elem, setAbsolutePosition) {
    var elementHeight, elementWidth;

    elementHeight = elem.style.height || elem.clientHeight;
    elementWidth = elem.style.width || elem.clientWidth;

    if (setAbsolutePosition === true) {
        // Absolutly position the element.
        elem.style.position = "absolute";
        elem.style.top = ((window.innerHeight - parseInt(elementHeight, 10)) / 2) + 'px';
        elem.style.left = ((window.innerWidth - parseInt(elementWidth, 10)) / 2) + 'px';

    } else {
        elem.style.marginTop = ((window.innerHeight - parseInt(elementHeight, 10)) / 2) + 'px';
    }
};

//commonFun.verticallyCenterElemInWindow = function (elem) {
//    var elementHeight;
//
//    elementHeight = parseInt(elem.offsetHeight || elem.clientHeight, 10);
//
//    elem.style.marginTop = ((window.innerHeight - parseInt(elementHeight, 10)) / 2) + 'px';
//};

commonFun.verticallyCenterElemInParent = function (elem, parent) {
    var elementHeight, parentHeight;

    elementHeight = parseInt(elem.offsetHeight || elem.clientHeight, 10);
    parentHeight = parseInt(parent.offsetHeight, 10);

    elem.style.marginTop = ((parentHeight - elementHeight) / 2) + 'px';
};

commonFun.createImageElement = function (imageFilename, hideImage) {
    var elem;

    // Create the HTML img element.
    elem = document.createElement("img");
    // Set the source of the image.
    elem.src = imageFilename;
    // Set the height and width to be no larger then it's container.
    elem.style.maxWidth = "100%";
    elem.style.maxHeight = "100%";

    if (hideImage === true) {
        // Set the image display so it will be hidden.
        elem.style.display = "none";
    }

    return elem;
};

commonFun.createVideoElement = function (videoFilename, hideElem, type) {
    var elem;

    // Create the HTML img element.
    elem = document.createElement("video");
    // Set the source of the image.
    elem.src = videoFilename;
    // Set the height and width to be no larger then it's container.
    elem.style.maxWidth = "100%";
    elem.style.maxHeight = "100%";

    if (type) {
        elem.type = type;
    }

    if (hideElem === true) {
        // Set the image display so it will be hidden.
        elem.style.display = "none";
    }

    return elem;
};

commonFun.createAudioElement = function (audioFilename) {
    var elem;

    elem = document.createElement("audio");
    elem.src = audioFilename;
    /*  elem.preload = "auto"; */
    elem.volume = "1.0";

    return elem;
};

// Returns 'count' non-repeating random numbers in the range of 1 to 'maxValue'.
commonFun.nonRepRandPerm = function (maxValue, count) {
    var result, num, size;

    if (count > maxValue) {
        console.warn('Warning: the "count" argument is bigger than the "maxValue" argument.');
    } else if (count === 1) {
        result = commonFun.getRandomInt(1, maxValue);
    } else {
        result = [];
        size = 0;

        while (size < count) {
            num = commonFun.getRandomInt(1, maxValue);

            if (result.indexOf(num) === -1) {
                result.push(num);
                size += 1;
            }
        }
    }

    return result;
};

// Returns a random integer between min and max
// Using Math.round() will give you a non-uniform distribution!
commonFun.getRandomInt = function (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

commonFun.shuffleArray = function (array) {
    var result, i, shuffledIndices, index;

    result = [];

    shuffledIndices = commonFun.nonRepRandPerm(array.length, array.length);

    if (!Array.isArray(shuffledIndices)) {
        shuffledIndices = [shuffledIndices];
    }

    for (i = 0; i < array.length; i++) {
        index = shuffledIndices[i] - 1;
        result.push(array[index]);
    }

    return result;
};

// This function expects to get data in a specific format.
commonFun.sendExpResultsToServer = function (experimentName, experimenterName, subjectId, results, callbackFun) {
    var postData;

    // Validate that none of the arguments is null, undefind or an empty string
    // and only than send the results to the server.
    if (!experimentName) {
        console.warn('Cannot send the experiments results to the server. "experimentName" is missing or empty !');
    } else if (!experimenterName) {
        console.warn('Cannot send the experiments results to the server. "experimenterName" is missing or empty !');
    } else if (!subjectId) {
        console.warn('Cannot send the experiments results to the server. "subjectId" is missing or empty !');
    } else if (!results) {
        console.warn('Cannot send the experiments results to the server. "results" is missing or empty !');
    } else {
        postData = $.ajax({
            type: "POST",
            url: commonFun.getRouterURL(),
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            /*  contentType: 'application/json', */
            timeout: 15000,
            data: {
                action: 'insertExperimentResultsToDb',
                experimentName: experimentName,
                experimenterName: experimenterName,
                subjectId: subjectId,
//                results: JSON.stringify(results)
                results: results
                        // results: results.toString()
            }
        });

        postData.always(function () {
            commonFun.areResultsInDB(experimentName, experimenterName, subjectId, callbackFun);
        });
    }
};

commonFun.getFilenameFromFullName = function (fullName) {
    if (fullName) {
        return fullName.replace(/^.*[\\\/]/, '');

    } else {
        return null;
    }
};

commonFun.checkIfBrowserIsGoogleChrome = function () {
    var isChromium = window.chrome,
            vendorName = window.navigator.vendor;

    if (isChromium && vendorName === "Google Inc.") {
        return true;
    } else {
        return false;
    }
};

// Returns the first index where str appears in strArray, or -1 if it's not
// found.
// Note: the function doesn't check for an exact match, but any match.
// i.e., if str="abc" and strArray contains an entry of "45abc4", it is
// considered as a valid match.
commonFun.searchForStringInArray = function (str, strArray) {
    var j;

    for (j = 0; j < strArray.length; j++) {
        if (strArray[j].match(str)) {
            return j;
        }
    }

    return -1;
};

// Returns the number of milliseconds elapsed since either the browser navigationStart event or
// the UNIX epoch, depending on availability.
// Where the browser supports 'performance' we use that as it is more accurate (microsoeconds
// will be returned in the fractional part) and more reliable as it does not rely on the system time.
// Where 'performance' is not available, we will fall back to Date().getTime().
// Jonathan - see http://dvolvr.davidwaterston.com/2012/06/24/javascript-accurate-timing-is-almost-here/
commonFun.now = function () {
    var performance = window.performance || {};

    performance.now = (function () {
        return performance.now ||
                performance.webkitNow ||
                performance.msNow ||
                performance.oNow ||
                performance.mozNow ||
                function () {
                    return new Date().getTime();
                };
    })();

    return performance.now();
};

// Starts a timer. To get the time, call commonFun.stopTimer().
commonFun.startTimer = function () {
    // Create the time object if it doesn't exist.
    commonFun.timerObj = commonFun.timerObj || {};

    commonFun.timerObj.start = commonFun.now();
    commonFun.timerObj.isRunning = true;
};

// Stops the time that started when commonFun.startTimer() was called, and returns the
// elapsed time.
commonFun.stopTimer = function () {
    if (!commonFun.timerObj) {
        console.warn("commonFun.timerObj doesn't exist. Cannot call commonFun.stopTimer() !");
    } else {
        commonFun.timerObj.isRunning = false;
        commonFun.timerObj.lastElapsedTime = commonFun.now() - commonFun.timerObj.start;

        return Math.round(commonFun.timerObj.lastElapsedTime);
    }
};

// Puts the browser in full screen mode.
commonFun.enterFullScreenMode = function () {
    var elm, reqFullScreen;

    // Get the document element.
    elm = document.documentElement;
    // Try and get the correct function for the browser.
    reqFullScreen = elm.requestFullScreen || elm.webkitRequestFullScreen || elm.mozRequestFullScreen;

    if (elm.webkitRequestFullScreen) {
        elm.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
    } else {
        reqFullScreen.call(elm);
    }
};

// Returns true if the browser is in full screen mode.
commonFun.isBrowserInFullScreenMode = function () {
    if (window.innerHeight === screen.height || window.innerHeight - 30 >= screen.height) {
        return true;
    } else {
        return false;
    }
};

// Disable the context menu for a specific element.
commonFun.disableContextMenuForElm = function (element) {
    element.addEventListener('contextmenu', function (evt) {
        evt.preventDefault();
    }
    );
};

commonFun.areResultsInDB = function (experimentName, experimenterName, subjectId, callbackFun) {
    var postData;

    postData = $.ajax({
        type: "POST",
        url: commonFun.getRouterURL(),
        timeout: 10000,
        data: {
            action: 'areResultsInDB',
            experimentName: experimentName,
            experimenterName: experimenterName,
            subjectId: subjectId
        }
    });

    // When we have received the filenames, parse the respose as a JSON
    // response and call callbackFun.
    postData.always(function (response) {
        var result;

        result = false;

        if (response) {
            try {
                result = JSON.parse(response);
                result = parseInt(result, 10) > 0;
            } catch (e) {
                console.log(e);
            }
        }

        callbackFun(result);
    });
};

// Gets the url for the back end router.
commonFun.getRouterURL = function () {
    var hostname;

    hostname = window.location.hostname;

    if (hostname === "milka" || hostname === "milka.tau.ac.il") {
        return "../router.php";
    } else {
        return "http://xegozi.tau.ac.il/experiments/router.php";
    }
};

commonFun.registerSubject = function (subjectId, experimentName, experimenterName, callback) {
    var postData;

    postData = $.ajax({
        type: "POST",
        url: commonFun.getRouterURL(),
        timeout: 10000,
        data: {
            action: 'registerSubjectToExp',
            experimentName: experimentName,
            experimenterName: experimenterName,
            subjectId: subjectId
        }
    });

    postData.done(function (response) {
        var result;

        result = JSON.parse(response);
        result = parseInt(result, 10) > 0;

        if (callback) {
            callbackFun(result);
        }
    });
};

commonFun.runGenericEndOfExpRoutine = function (experimentName, experimenterName, subjectId, results, callback) {
    var messageDiv, endOfExperimentCallback, confirmationCodeDiv, errorMessage;
    // Clear all content from the page.
    document.querySelector("html").innerHTML = "";
    // Ensure we "give" the user his mouse cursor back.
    document.querySelector("html").style.cursor = "";

    // Add a call counter.
    commonFun.submitResultsCount = 0;

    // Show a loading message and icon.
    messageDiv = commonFun.getProcessingResultsMessageDiv();
    document.body.appendChild(messageDiv);

    // The function is defined here, so that the variables it uses will be in it's scope.
    endOfExperimentCallback = function (didSucceed) {
        if (didSucceed === true) {
            // if we got a callback function, we'll call it.
            if (callback) {
                // Clear all content from the page.
                document.querySelector("html").innerHTML = "";

                callback(didSucceed);
            } else {
                commonFun.getSubjectConfirmationCode(subjectId, experimentName, commonFun.endOfExperimentPhase2);
            }
        } else if (commonFun.submitResultsCount < 3) { // In case submitting results failed.
            // Send the results to the server again.
            commonFun.sendExpResultsToServer(experimentName, experimenterName, subjectId, results, endOfExperimentCallback);
            // Increment the call counter.
            commonFun.submitResultsCount += 1;
        } else {
            commonFun.getSubjectConfirmationCode(subjectId, experimentName, commonFun.endOfExperimentPhase2);
            errorMessage = "ERROR when sending experiment results to server. ** ";
            errorMessage += " experimentName: " + experimenterName + " ** " + results.toString();
            commonFun.logErrorFromClientSideToServerErrorLog(errorMessage);
        }
    };

    // Send the results to the server
    commonFun.sendExpResultsToServer(experimentName, experimenterName, subjectId, results, endOfExperimentCallback);
};

commonFun.endOfExperimentPhase2 = function (confirmationCode) {
    var messageDiv, confirmationCodeDiv;

    // Clear all content from the page.
    document.querySelector("html").innerHTML = "";
    // Show an end of experiment message.
    messageDiv = commonFun.getEndOfExperimentMessageDiv();
    document.body.appendChild(messageDiv);

    if (confirmationCode !== false) {
        // Display the confirmation code.
        confirmationCodeDiv = commonFun.getConfirmationCodeDiv(confirmationCode);
        // Add it to the page.
        document.body.appendChild(confirmationCodeDiv);
    }
};

commonFun.getProcessingResultsMessageDiv = function () {
    var result;

    result = document.createElement('div');

    result.innerHTML =
            '<div style="margin: 20% auto; text-align: center;">' +
            '<h2>Please wait while we process the results.</h2>' +
            '<br>' +
            '<img style="height: 50px;" src="../common/loading.gif"/>' +
            '</div>';

    return result;
};

commonFun.getEndOfExperimentMessageDiv = function () {
    var result;

    result = document.createElement('div');

    result.innerHTML =
            '<div id="experimentEndedDiv" style="margin: 20% auto -100px; text-align: center;">' +
            '<h2>Thank you for participating !</h2>' +
            '</div>';

    return result;
};

commonFun.getConfirmationCodeDiv = function (confirmationCode) {
    var result;

    result = document.createElement('div');

    result.innerHTML =
            '<div id="confirmationCodeDiv" style="margin: 20% auto; text-align: center;">' +
            '<h2>Please copy the confirmation code and paste it in the proper place at the Mechanical Turk website:</h2>' +
            '<h1 style="font-weight: bold;">' + confirmationCode + '</h1>' +
            '<br>' +
            '<br>' +
            '<h3>You can press F11 or the Esc key to exit full screen mode.</h3>' +
            '</div>';

    return result;
};

commonFun.getSubjectConfirmationCode = function (subjectId, experimentName, callback) {
    var postData;

    postData = $.ajax({
        type: "POST",
        url: commonFun.getRouterURL(),
        timeout: 10000,
        data: {
            action: 'getSubjectConfirmationCode',
            subjectId: subjectId,
            experimentName: experimentName
        }
    });

    postData.done(function (response) {
        var result, code;

        result = JSON.parse(response);

        if (result.length > 0) {
            code = result[0].confirmation_code;
        } else {
            code = false;
        }

        callback(code);
    });
};

commonFun.displayUseGoogleChromeMessage = function () {
    document.querySelector("html").innerHTML =
            '<div style="margin: 20% auto; text-align: center;">' +
            '<h2>You must use google chrome to participate in this experiment.</h2><h3>Thank you.</h3>' +
            '</div>';
};

// Source: http://css-tricks.com/snippets/javascript/get-url-variables/
commonFun.getQueryVariable = function (variableName) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");

    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variableName) {
            return pair[1];
        }
    }

    return(false);
};

commonFun.displayRejectExperimentMessage = function () {
    document.querySelector("html").innerHTML =
            '<div style="margin: 20% auto; text-align: center;">' +
            '<h2>We\'re sorry to inform you...</h2><h3>Thank you.</h3>' +
            '</div>';
};

commonFun.keycodeToKeyname = function (keyCode) {
    var keyPressed;

    if (keyCode === 48 || keyCode === 96) {
        keyPressed = 0;
    } else if (keyCode === 49 || keyCode === 97) {
        keyPressed = 1;
    } else if (keyCode === 50 || keyCode === 98) {
        keyPressed = 2;
    } else if (keyCode === 51 || keyCode === 99) {
        keyPressed = 3;
    } else if (keyCode === 52 || keyCode === 100) {
        keyPressed = 4;
    } else if (keyCode === 53 || keyCode === 101) {
        keyPressed = 5;
    } else if (keyCode === 54 || keyCode === 102) {
        keyPressed = 6;
    } else if (keyCode === 55 || keyCode === 103) {
        keyPressed = 7;
    } else if (keyCode === 56 || keyCode === 104) {
        keyPressed = 8;
    } else if (keyCode === 57 || keyCode === 105) {
        keyPressed = 9;
    } else if (keyCode === 89) {
        keyPressed = 'y';
    } else if (keyCode === 78) {
        keyPressed = 'n';
    } else if (keyCode === 80) {
        keyPressed = 'p';
    } else if (keyCode === 87) {
        keyPressed = 'w';
    } else if (keyCode === 75) {
        keyPressed = 'k';
    } else if (keyCode === 68) {
        keyPressed = 'd';
    } else if (keyCode === 70) {
        keyPressed = 'f';
    } else if (keyCode === 32) {
        keyPressed = 'space';
    } else {
        keyPressed = 'Wrong key!';
    }

    return keyPressed;
};

commonFun.arrayToCSVString = function (arr) {
    var result, i, j;

    result = "";

    for (i = 0; i < arr.length; i++) {
        for (j = 0; j < arr[i].length; j++) {
            result += arr[i][j] + ",";
        }

        result += "\n";
    }

    return result;
};

commonFun.openDownloadResultsAsCSVDialog = function (subjectId, resultsArray) {
    var link, data, stringObj;

    stringObj = commonFun.arrayToCSVString(resultsArray);

    // Add a timestamp and the subject's id.
    stringObj += Date().toString() + "," + subjectId;

    data = "text/csv;charset=utf-8," + encodeURIComponent(stringObj);
    link = document.createElement('a');

    link.id = "myLinkId";
    link.href = 'data:' + data;
    link.download = subjectId + ".csv";
    link.innerText = "Click here if the download hasn't started";
    link.style.fontSize = "40px";
    link.style.margin = "250px auto";
    link.style.display = "block";
    link.style.textAlign = "center";

    document.body.appendChild(link).click();
};

// Gets the names of all of a subject's experiments.
// Pass "true" as the justNames parameter to get only the names of the experiments.
// Otherwise, you will get a list of all the subject's experiments and the number
// of times he participated in each of them.
commonFun.getSubjectExperiments = function (subjectId, callback, justNames) {
    var postData;

    postData = $.ajax({
        type: "POST",
        url: commonFun.getRouterURL(),
        timeout: 10000,
        data: {action: 'getSubjectExperiments', subjectId: subjectId}
    });

    postData.done(function (response) {
        var result, names, i;

        if (response) {
            result = JSON.parse(response);

            if (justNames === true) {
                names = [];

                for (i = 0; i < result.length; i++) {
                    names.push(result[i].experiment_name);
                }

                callback(names);
            } else {
                callback(result);
            }

        } else {
            callback([]);
        }
    });
};

// Get the results of an experiment (all of them) by passing the experiments name.
commonFun.getExperimentResultsByName = function (expName, callback) {
    var postData;

    postData = $.ajax({
        type: "POST",
        url: commonFun.getRouterURL(),
        timeout: 10000,
        data: {action: 'getExperimentResultsByName', expName: expName}
    });

    postData.done(function (response) {
        var result, names, i;

        result = JSON.parse(response);

        callback(result);

    });
};

commonFun.removeEmptyEntriesFromArray = function (arr) {
    var result;

    result = arr.filter(function (n) {
        return n;
    });

    result = result.map(function (n) {
        return n.trim();
    });

    result = result.filter(function (n) {
        return n;
    });

    return result;
};

commonFun.openDownloadResultsAsCSVDialog = function (logFilename, subjectId, resultsArray) {

    var link, data, stringObj;

    stringObj = commonFun.arrayToCSVString(resultsArray);

    // Add a timestamp and the subject's id.
    stringObj += Date().toString() + "," + subjectId;

    data = "text/csv;charset=utf-8," + encodeURIComponent(stringObj);
    link = document.createElement('a');

    link.id = "myLinkId";
    link.href = 'data:' + data;
    link.download = subjectId + ".csv";
    link.innerText = "Click here if the download hasn't started";
    link.style.fontSize = "25px";
    link.style.margin = "250px auto";
    link.style.display = "block";
    link.style.textAlign = "center";

    document.body.innerHTML = "";
    document.body.appendChild(link).click();
};

commonFun.logErrorFromClientSideToServerErrorLog = function (message, callback) {
    var postData;

    // Add a message prefix
    message = "ERROR SENT FROM THE CLIENT-SIDE: " + message;

    postData = $.ajax({
        type: "POST",
        url: commonFun.getRouterURL(),
        timeout: 10000,
        data: {action: 'logErrorFromClientSideToServerErrorLog', message: message}
    });

    postData.done(function (response) {
        var result, names, i;

        result = JSON.parse(response);

        if (callback) {
            callback(result);
        } else {
            try {
                console.log('logErrorFromClientSideToServerErrorLog result: ', result);
            } catch (e) {
                // Do nothing.
            }
        }
    });
};

commonFun.isEmailValid = function (email, callback) {
    var postData;

    postData = $.ajax({
        type: "POST",
        url: commonFun.getRouterURL(),
        timeout: 10000,
        data: {action: 'isEmailValid', emailAddress: email}
    });

    postData.done(function (response) {
        var result, names, i;

        result = JSON.parse(response);

        if (callback) {
            callback(result);
        } else {
            try {
                console.log('isEmailValid result: ', result);
            } catch (e) {
                // Do nothing.
            }
        }
    });
};

commonFun.searchForStringInArray = function (str, strArray) {
    for (var j = 0; j < strArray.length; j++) {
        if (strArray[j].match(str)) {
            return j;
        }
    }

    return -1;
};

commonFun.isKeyBetweenNumToNum = function (keyPressed, lowerBound, upperBound) {

    // If keyPressed isn't an integer, return false
    try
    {
        parseInt(keyPressed);
    } catch (e) {
        return false;
    }

    if (isNaN(parseInt(keyPressed, 10)) || isNaN(parseInt(lowerBound, 10)) || isNaN(parseInt(upperBound, 10))) {
        console.error("one of the arguments cannot be parsed as an int. keyPressed, lowerBound, upperBound: ",
                keyPressed, lowerBound, upperBound);

    } else {
        return parseInt(keyPressed, 10) >= parseInt(lowerBound, 10) &&
                parseInt(keyPressed, 10) <= parseInt(upperBound, 10);
    }

};

commonFun.addLoadingDiv = function () {
    var div, loadingImage, statusElement;

    loadingImage = "../common/loading.gif";
    div = document.createElement('div');
    statusElement = document.createElement('h3');

    div.id = "loadingDataDiv";
    div.style.margin = "20% auto";
    div.style.textAlign = "center";
    statusElement.id = "loadingDataStatusElement";

    div.innerHTML =
            '<h2>Loading data ...</h2>' +
            '<br>' +
            '<img style="height: 50px;" src="' + loadingImage + '"/>';

    div.appendChild(statusElement);

    document.body.insertBefore(div, document.body.firstChild);

    return statusElement;
};

commonFun.removeLoadingDiv = function () {
    document.querySelector('#loadingDataDiv').style.display = "none";
};

// Arguments:
//
// mediaElements argument should be a either a string, or an array.
// If it's a string: "video", "img" or "audio".
// If it's an array: ["video", "img"] or any combination of the above strings in an array.
//
// callback is optional, and is called one all media is ready.
//
// readyStatusElem is optional. it's an HTML element whose inner text will be updated with the
// percentage of complete media items.
commonFun.ensureAllMediaHasDownloaded = function (mediaElements, callback, readyStatusElem, elementsArray) {
    var allElements, totalNumElements, j, nodeListAsArray;

    allElements = [];

    if (!Array.isArray(mediaElements)) {
        mediaElements = [mediaElements];
    }

    if (elementsArray && elementsArray.length && elementsArray.length > 0) {
        allElements = elementsArray;

    } else {
        for (j = 0; j < mediaElements.length; j++) {
            nodeListAsArray = Array.prototype.slice.call(document.querySelectorAll(mediaElements[j]));
            allElements = allElements.concat(nodeListAsArray);
        }
    }

    totalNumElements = allElements.length;

    console.assert(totalNumElements !== 0, 'no media elements of type ' + mediaElements + ' were found !');

    function checkIfReady() {
        var i, allAreReady, numReadyItems, currElem;

        allAreReady = true;
        numReadyItems = 0;

        for (i = 0; i < allElements.length; i++) {
            currElem = allElements[i];

            if (currElem.nodeName === "IMG") {
                if (!currElem.complete || currElem.naturalWidth === 0) {
                    allAreReady = false;
                } else {
                    numReadyItems += 1;
                }
            } else if (currElem.nodeName === "VIDEO" || currElem.nodeName === "AUDIO") {
                if (currElem.readyState !== 4) {
                    allAreReady = false;
                } else {
                    numReadyItems += 1;
                }
            }
        }

        if (allAreReady === false) {
            if (readyStatusElem) {
                readyStatusElem.innerText = parseInt(((numReadyItems / totalNumElements) * 100), 10) + '%';
            }

            setTimeout(checkIfReady, 500);
        } else {
            console.log(mediaElements + ' elements are ready');

            if (callback) {
                callback();
            } else {
                console.log('All elements of type ' + mediaElements + ' are ready !');
            }
        }
    }

    checkIfReady();
};

commonFun.isElementHidden = function (elm) {
    return (elm.offsetParent === null);
};

commonFun.isElementVisible = function (elm) {
    return !commonFun.isElementHidden(elm);
};

commonFun.newAPITest = function (str) {
    var postData;

    postData = $.ajax({
        type: "POST",
        url: 'http://milka/expadmin/expapi/test',
        timeout: 10000,
        data: {
            action: 'registerSubjectToExp',
            experimentName: 'experimentName',
            experimenterName: 'experimenterName',
            subjectId: 'subjectId'
        }
    });

    postData.done(function (response) {
        console.log(response);
    });
};


commonFun.getP4AData = function () {
    var result;

    result = {};

    result.userID = commonFun.getQueryVariable('userID');
    result.surveyID = commonFun.getQueryVariable('surveyID');

    return result;
};

commonFun.countSubjectsForExperiment = function (expName, callback) {
    var postData;

    postData = $.ajax({
        type: "POST",
        url: commonFun.getRouterURL(),
        timeout: 10000,
        data: {action: 'getExperimentResultsByName', expName: expName}
    });

    postData.done(function (response) {
        var count, i, results, testSubPrefixRegex;

        testSubPrefixRegex = "^" + commonFun.CONSTS.testSubjectPrefix + ".*";

        results = JSON.parse(response);

        count = 0;

        for (i = 0; i < results.length; i++) {
            if (results[i].subject_id.match(testSubPrefixRegex) === null) {
                count++;
            }
        }

        callback(count);
    });
};

commonFun.showQuotaFullMessageAndRedirect = function (p4aData, timeToWait) {
    var message, href;

    message = document.createElement('h1');

    href = "http://www.panel4all.co.il/survey_runtime/external_survey_status.php?" +
            "surveyID=" + p4aData.surveyID +
            "&userID=" + p4aData.userID +
            "&status=overquota";

    message.innerText = "ניסוי זה נסגר. בעוד מספר רגעים תנותבו בחזרה ל- panel4all.";
    message.style.margin = "20% auto";
    message.style.direction = "rtl";
    message.style.display = "block";
    message.style.textAlign = "center";


    document.body.innerHTML = "";
    document.body.appendChild(message);

    setTimeout(function () {
        window.location = href;
    }, timeToWait);
};


commonFun.showP4AFinishedExperimentMessageAndRedirect = function (p4aData, timeToWait) {
    var message, href;

    message = document.createElement('h1');
    message.innerText = "תודה לכם על ההשתתפות בניסוי. בעוד מספר רגעים תנותבו בחזרה ל- panel4all.";
    message.style.margin = "20% auto";
    message.style.direction = "rtl";
    message.style.display = "block";
    message.style.textAlign = "center";

    document.body.innerHTML = "";
    document.body.appendChild(message);

    href = "http://www.panel4all.co.il/survey_runtime/external_survey_status.php?" +
            "surveyID=" + p4aData.surveyID +
            "&userID=" + p4aData.userID +
            "&status=finish";

    setTimeout(function () {
        window.location = href;
    }, timeToWait);
};

commonFun.saveResultsAsCSVDuringExperiment = function (logFilename, subjectId, resultsArray) {
    var link, data, stringObj;

    // To allow saving the results multiple times as the experiment continues.
    $('#downloadResultsLink').remove();

    stringObj = commonFun.arrayToCSVString(resultsArray);

    // Add a timestamp and the subject's id.
    stringObj += Date().toString() + "," + subjectId;

    data = "text/csv;charset=utf-8," + encodeURIComponent(stringObj);
    link = document.createElement('a');

    link.id = "downloadResultsLink";
    link.href = 'data:' + data;
    link.download = logFilename + ".csv";
    link.style.fontSize = "25px";
    link.style.margin = "250px auto";
    link.style.display = "none";
    link.style.textAlign = "center";

    document.body.appendChild(link);
    document.querySelector('#downloadResultsLink').click();
};

commonFun.doesFileExist = function (urlToFile) {
    var xhr = new XMLHttpRequest();

    xhr.open('HEAD', urlToFile, true);
    xhr.onload = commonFun.doesFileExistCallbackHandler;
    xhr.onerror = commonFun.doesFileExistCallbackHandler;
    xhr.send();
};

commonFun.doesFileExistCallbackHandler = function (response) {
    if (this.status == "404") {
        console.error(this.responseURL + ' does not exist !');
    } else {
        console.log(this.responseURL + ' exists.');
    }
};

commonFun.randomBool = (function () {
    var a = new Uint8Array(1);

    return function () {
        crypto.getRandomValues(a);
        return a[0] < 127;
    };

})();

commonFun.randomstring = function (length) {
    var s = '';

    var randomchar = function () {
        var n = Math.floor(Math.random() * 62);
        if (n < 10)
            return n; //1-10
        if (n < 36)
            return String.fromCharCode(n + 55); //A-Z
        return String.fromCharCode(n + 61); //a-z
    };

    while (s.length < length)
        s += randomchar();
    return s;
}