function resT = parse_fm_res(dirName)
flist = dir(fullfile(dirName,'*.txt'));
numF = size(flist,1);
for f=1:numF
    flist(f).name
    fid = fopen(flist(f).name);
    a = fscanf(fid,'%s');
    colon = findstr(a,':');
    dashes = findstr(a,'-');
    commas = findstr(a,',');
    res = nan(6,30);
    counters = ones(1,30);
    % first result - after colon
    pic = str2num(a(colon+1:dashes(1)-1));
    reply = a(dashes(1)+1:commas(1)-1);
    cnt = counters(pic);
    if(strcmp(reply,'c'))
        res(cnt,pic) = 1;
    else
        res(cnt,pic) = 0;
    end
    counters(pic) = cnt + 1;
    for i=2:size(dashes,2)
        pic = str2num(a(commas(i-1)+1:dashes(i)-1))
        reply = a(dashes(i)+1:commas(i)-1);
        cnt = counters(pic)
        if(strcmp(reply,'c'))
            res(cnt,pic) = 1;
        else
            res(cnt,pic) = 0;
        end
        counters(pic) = cnt + 1;
    end
    fclose(fid);
    resT{f} = res;
end
